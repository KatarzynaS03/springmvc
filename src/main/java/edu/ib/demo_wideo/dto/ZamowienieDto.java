package edu.ib.demo_wideo.dto;

import edu.ib.demo_wideo.repo.entity.Customer;
import edu.ib.demo_wideo.repo.entity.Product;
import lombok.Data;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
public class ZamowienieDto {

    private Long ord_id;
    private Customer customer;
    private Set<Product> product = new HashSet<>();
    private LocalDate placeDate;
    private String status;
}
