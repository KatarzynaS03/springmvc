package edu.ib.demo_wideo.repo.entity;

import javax.persistence.*;

@Entity
@Table(name = "Klient")
public class Customer{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String addres;

    public Customer(Long id, String name, String addres) {
        this.id = id;
        this.name = name;
        this.addres = addres;
    }

    public Customer(String name, String addres) {
        this.name = name;
        this.addres = addres;
    }

    public Customer(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", addres='" + addres + '\'' +
                '}';
    }
}
