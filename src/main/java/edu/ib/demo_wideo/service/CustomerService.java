package edu.ib.demo_wideo.service;

import edu.ib.demo_wideo.repo.CustomerRepository;
import edu.ib.demo_wideo.repo.entity.Customer;
import edu.ib.demo_wideo.repo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> FindById(Long id){
        return customerRepository.findById(id);
    }

    public Iterable<Customer> findALL(){
        return customerRepository.findAll();
    }
    public Customer Save(Customer customer){
        return customerRepository.save(customer);
    }

    public void delete(Long id){
        customerRepository.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        Save(new Customer(1L,"Mariola Paździoch","Peronowa 1 i 3/4"));
        Save(new Customer(2L,"Marian Pażdzioch","Peronowa 1 i 3/4"));

    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }
}
