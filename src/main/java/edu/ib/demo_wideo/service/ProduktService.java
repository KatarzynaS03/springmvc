package edu.ib.demo_wideo.service;

import edu.ib.demo_wideo.repo.ProductRepository;
import edu.ib.demo_wideo.repo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class ProduktService {

    private ProductRepository productRepository;

    @Autowired
    public ProduktService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Optional<Product> FindById(Long id){
        return productRepository.findById(id);
    }

    public Iterable<Product> findALL(){
        return productRepository.findAll();
    }
    public Product Save(Product videoCassette){
        return productRepository.save(videoCassette);
    }

    public void delete(Long id){
        productRepository.deleteById(id);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        Save(new Product(1L,"Honda papieska", 1000, true));
        Save(new Product(2L,"Toyota Avensis Verso", 1237, false));

    }

    public Product save(Product product) {
        return productRepository.save(product);
    }
}
