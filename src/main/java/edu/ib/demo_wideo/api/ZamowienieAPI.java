package edu.ib.demo_wideo.api;

import edu.ib.demo_wideo.dto.ZamowienieDto;
import edu.ib.demo_wideo.repo.ZamowienieRepository;
import edu.ib.demo_wideo.repo.entity.Zamowienie;
import edu.ib.demo_wideo.service.ZamowieniaService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ZamowienieAPI {

    private ZamowieniaService zamowieniaService;

    @Autowired
    public ZamowienieAPI (ZamowieniaService zamowieniaService) {
        this.zamowieniaService = zamowieniaService;
    }

    @GetMapping("/zamowienia/all")
    public Iterable<Zamowienie> getALL(){

        return zamowieniaService.findALL();
    }
    @GetMapping("/zamowienia")
    public Optional<Zamowienie> getById(@RequestParam Long index){
        return zamowieniaService.FindById(index);
    }

    @PostMapping("/admin/zamowienia")
    public Zamowienie addVIdeo(@RequestBody Zamowienie zamowienie){
        return zamowieniaService.Save(zamowienie);
    }

    @PutMapping("/admin/zamowienia")
    public Zamowienie updateVideo(@RequestBody Zamowienie zamowienie){
        return zamowieniaService.Save(zamowienie);
    }
    @DeleteMapping("/zamowienia")
    public void deleteVideo(@RequestParam Long index){
        zamowieniaService.delete(index);
    }

    @PatchMapping("/admin/zamowienia")
    public void patchProduct(
            @RequestParam Long index,
            @RequestBody ZamowienieDto zamowienieDto)  {
        Zamowienie zamowienie = zamowieniaService
                .FindById(index).orElseThrow(RuntimeException::new);

        boolean needUpdate = false;

        if (zamowienieDto.getCustomer()!= null) {
            zamowienie.setCustmer(zamowienieDto.getCustomer());
            needUpdate = true;
        }

        if (!zamowienieDto.getProduct().isEmpty()) {
            zamowienie.setProdukt(zamowienieDto.getProduct());
            needUpdate = true;
        }

        if (zamowienieDto.getPlaceDate() != null) {
            zamowienie.setPlaceDate(zamowienieDto.getPlaceDate());
            needUpdate = true;
        }

        if (StringUtils.hasLength(zamowienieDto.getStatus())) {
            zamowienie.setStatus(zamowienieDto.getStatus());
            needUpdate = true;
        }

        if (needUpdate) {
            zamowieniaService.save(zamowienie);
        }
    }

}
