package edu.ib.demo_wideo.api;

import edu.ib.demo_wideo.dto.CustomerDto;
import edu.ib.demo_wideo.repo.CustomerRepository;
import edu.ib.demo_wideo.repo.entity.Customer;
import edu.ib.demo_wideo.service.CustomerService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerAPI {
    private CustomerRepository customerRepository;
    private CustomerService customerService;

    @Autowired
    public CustomerAPI (CustomerService customerService, CustomerRepository customerRepository) {
        this.customerService = customerService;
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getALL(){

        return customerService.findALL();
    }
    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long index){
        return customerService.FindById(index);
    }

    @PostMapping("/admin/customer")
    public Customer addVIdeo(@RequestBody Customer customer){
        return customerService.Save(customer);
    }

    @PutMapping("/admin/customer")
    public Customer updateVideo(@RequestBody Customer customer){
        return customerService.Save(customer);
    }

    @DeleteMapping("/customer")

    public void deleteVideo(@RequestParam Long index){
        customerService.delete(index);
    }

    @PatchMapping("/admin/customer")
    public void patchCustomer(
            @RequestParam Long index,
            @RequestBody CustomerDto customerDto)  {
        Customer customer = customerService
                    .FindById(index).orElseThrow(RuntimeException::new);

            boolean needUpdate = false;

            if (StringUtils.hasLength(customerDto.getName())) {
                customer.setName(customerDto.getName());
                needUpdate = true;
            }

            if (StringUtils.hasLength(customerDto.getAddres())) {
                customer.setAddres(customerDto.getAddres());
                needUpdate = true;
            }
            if (needUpdate) {
                customerService.save(customer);
            }
        }

}
